﻿using System;
using System.IO;
using Xunit;

namespace RCorpFoodPricerTest
{
    public class UnitTest1
    {
[Fact]
        public void GolderMasterAppCompute()
        { 
            //  string[] types = {"assiette", "other", ""};
            //  string[] names = {"", "name"};
            //  string[] sizes = {"petit", "moyen", "grand"};
            //  string[] dsizes = {"normal", "notnormal"};
            //  string[] coffees = {"yes", "no"};
            //
            //  string result = "";
            //
            //  for (int i = 0; i < 100; i++)
            //  {
            //      Random rnd = new Random();
            //      var randTypeIndex = rnd.Next(types.Length);
            //      var randNameIndex = rnd.Next(names.Length);
            //      var randSizeIndex = rnd.Next(sizes.Length);
            //      var randDSizeIndex = rnd.Next(dsizes.Length);
            //      var randCoffeeIndex = rnd.Next(coffees.Length);
            //
            //      using var writer = new StringWriter();
            //      Console.SetOut(writer);
            //      Console.SetError(writer);
            //      CurMain(new string[]
            //      {
            //          types[randTypeIndex], names[randNameIndex], null, sizes[randSizeIndex], null,
            //          dsizes[randDSizeIndex], coffees[randCoffeeIndex]
            //      });
            //      var sut = writer.ToString();
            //      Assert.Equal(sut, sut);
            //      result +=
            //          $"{types[randTypeIndex]};{names[randNameIndex]};{sizes[randSizeIndex]};{dsizes[randDSizeIndex]};{coffees[randCoffeeIndex]};{sut}" +
            //          "|";
            //  }
            //
            // File.WriteAllText("toto.txt", result);
            
            var test = File.ReadAllText("toto.txt");
            var lines = test.Split("|");
            
            for (int i = 0; i < lines.Length - 1; i++)
            {
                var allData = lines[i].Split(";");
                for (int j = 0; j < allData.Length; j++)
                {
                    var type = allData[0];
                    var name = allData[1];
                    var size = allData[2];
                    var dsize = allData[3];
                    var coffee = allData[4];
                    var expectedResult = allData[5];
                
                    using var writer = new StringWriter();
                    Console.SetOut(writer);
                    Console.SetError(writer);
                    CurMain(new []
                    {
                        type, name, null, size, null,
                        dsize, coffee
                    });
                
                    var sut = writer.ToString();
                    Assert.Equal(sut, expectedResult);
                }
            }
        }
    }
}