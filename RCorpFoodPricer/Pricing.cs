﻿using RCorpFoodPricer.Logger;
using RCorpFoodPricer.UpdateTotalPrice;

namespace RCorpFoodPricer
{
    public class App
    {
        //calcule le prix payé par l'utilisateur dans le restaurant, en fonction de type de 
        //repas qu'il prend (assiette ou sandwich), de la taille de la boisson (petit, moyen, grand), du dessert et
        //de son type (normal ou special) et si il prend un café ou pas (yes ou no).
        //les prix sont fixes pour chaque type de chose mais des réductions peuvent s'appliquer
        //si cela rentre dans une formule!
        private static class MealType
        {
            public const string Plate = "assiette";
            public const string Sandwich = "sandwich";
        }

        private static string WithCoffee = "yes";

        public double Compute(string type, string name, string size, string dsize, string coffee)
        {
            if (IsMealTypeAndNameMealEmpty(type, name)) return Price.ErrorPrice;

            var meal = new Meal(type, size, dsize, coffee);
            ILogger logger = new Logger.Logger();

            var totalPrice = meal.Type == MealType.Plate
                ? GetTotalPriceDependToDrinkSize(meal, logger, Price.PlatePrice)
                : GetTotalPriceDependToDrinkSize(meal, logger, Price.SandwichPrice);;

            return CheckIfFreeCoffeeOrAddCoffeePriceInTotal(meal, totalPrice, logger);
        }

        private bool IsMealTypeAndNameMealEmpty(string type, string name)
        {
            return string.IsNullOrEmpty(type + name);
        }

        private static int GetTotalPriceDependToDrinkSize(Meal meal, ILogger logger, int totalPrice)
        {
            IUpdateTotalPriceStrategy updateTotalPriceStrategy = UpdateTotalPriceFactory.build(meal.DrinkSize, logger);
            return updateTotalPriceStrategy.Update(meal.DessertSize, totalPrice);
        }

        private int CheckIfFreeCoffeeOrAddCoffeePriceInTotal(Meal meal, int currentTotalPrice, ILogger logger)
        {
            if (meal.Type == MealType.Plate && meal.DrinkSize == DrinkSize.Medium &&
                meal.DessertSize == DessertSize.Normal &&
                meal.Coffee == WithCoffee)
            {
                logger.Write(" avec café offert!");
                return currentTotalPrice;
            }

            return currentTotalPrice + Price.CoffeePrice;
        }
    }
}