﻿using System;

namespace RCorpFoodPricer.Logger
{
    public class Logger : ILogger
    {
        public void Write(string message)
        {
            Console.Write(message);
        }
    }
}