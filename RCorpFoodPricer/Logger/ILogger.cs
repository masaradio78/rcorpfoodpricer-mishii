﻿namespace RCorpFoodPricer.Logger
{
    public interface ILogger
    {
        void Write(string message);
    }
}