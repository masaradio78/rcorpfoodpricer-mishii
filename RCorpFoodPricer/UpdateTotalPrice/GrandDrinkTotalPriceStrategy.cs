﻿using RCorpFoodPricer.Logger;

namespace RCorpFoodPricer.UpdateTotalPrice
{
    public class GrandDrinkTotalPriceStrategy : IUpdateTotalPriceStrategy
    {
        private readonly ILogger _logger;
        public GrandDrinkTotalPriceStrategy(ILogger logger)
        {
            _logger = logger;
        }
        public int Update(string dessertSize, int totalPrice)
        {
            totalPrice += Price.GrandDrinkPrice + Price.NormalDessertPrice;
            if (dessertSize != DessertSize.Normal)
            {
                _logger.Write("Prix Formule Max appliquée ");
            }

            return totalPrice;
        }
    }
}