﻿using System;
using RCorpFoodPricer.Logger;

namespace RCorpFoodPricer.UpdateTotalPrice
{
    public class UpdateTotalPriceFactory
    {
        public static IUpdateTotalPriceStrategy build(string drinkSize, ILogger logger)
        {
            return drinkSize switch
            {
                DrinkSize.Small => new SmallDrinkTotalPriceStrategy(),
                DrinkSize.Medium => new MediumDrinkTotalPriceStrategy(logger),
                DrinkSize.Grand => new GrandDrinkTotalPriceStrategy(logger),
                _ => throw new ArgumentOutOfRangeException(nameof(drinkSize), drinkSize, null)
            };
        }
    }
}