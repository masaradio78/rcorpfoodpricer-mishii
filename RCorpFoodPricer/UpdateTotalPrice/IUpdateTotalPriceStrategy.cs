﻿using RCorpFoodPricer.Logger;

namespace RCorpFoodPricer.UpdateTotalPrice
{
    public interface IUpdateTotalPriceStrategy
    {
        int Update(string dessertSize, int totalPrice);
    }
}