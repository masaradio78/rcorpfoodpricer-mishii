﻿using RCorpFoodPricer.Logger;

namespace RCorpFoodPricer.UpdateTotalPrice
{
    public class MediumDrinkTotalPriceStrategy : IUpdateTotalPriceStrategy
    {
        private ILogger _logger;
        public MediumDrinkTotalPriceStrategy(ILogger logger)
        {
            _logger = logger;
        }

        public int Update(string dessertSize, int totalPrice)
        {
            totalPrice += Price.MediumDrinkPrice;
            if (dessertSize == DessertSize.Normal)
            {
                _logger.Write("Prix Formule Standard appliquée ");
            }
            else
            {
                totalPrice += Price.SpecialDessertPrice;
            }

            return totalPrice;
        }
    }
}