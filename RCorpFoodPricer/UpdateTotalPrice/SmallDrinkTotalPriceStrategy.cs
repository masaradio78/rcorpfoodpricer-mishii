﻿using Xunit;

namespace RCorpFoodPricer.UpdateTotalPrice
{
    public class SmallDrinkTotalPriceStrategy: IUpdateTotalPriceStrategy
    {
        public int Update(string dessertSize, int totalPrice)
        {
            if (dessertSize == DessertSize.Normal)
            {
                totalPrice += Price.NormalDessertPrice;
            }
            else
            {
                totalPrice += Price.SpecialDessertPrice;
            }
            return totalPrice + Price.SmallDrinkPrice;
        }
        
        [Fact]
        public void when_meal_dessert_size_is_normal_should_add_small_drink_and_normal_dessert_prices_in_total()
        {
            var total = 12;
            var dessertSize = DessertSize.Normal;

            var expectedTotalPrice = total + Price.SmallDrinkPrice + Price.NormalDessertPrice;

            Assert.Equal(Update(dessertSize, total), expectedTotalPrice);
        }

        [Fact]
        public void when_meal_dessert_size_is_not_normal_should_add_small_drink_and_special_dessert_prices_in_total()
        {
            var total = 65;
            var dessertSize = DessertSize.Special;

            var expectedTotalPrice = total + Price.SmallDrinkPrice + Price.SpecialDessertPrice;

            Assert.Equal(Update(dessertSize, total), expectedTotalPrice);
        }
    }
}
