﻿namespace RCorpFoodPricer
{
    public class Meal
    {
        public Meal(string type, string drinkSize, string dessertSize, string coffee)
        {
            Type = type;
            DrinkSize = drinkSize;
            DessertSize = dessertSize;
            Coffee = coffee;
        }

        public string Type { get; }
        public string DrinkSize { get; }
        public string DessertSize { get; }
        public string Coffee { get; }
    }
}