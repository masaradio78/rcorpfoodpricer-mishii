﻿namespace RCorpFoodPricer
{
    public static class DrinkSize
    {
        public const string Small = "petit";
        public const string Medium = "moyen";
        public const string Grand = "grand";
    }
}