﻿namespace RCorpFoodPricer
{
    public class Price
    {
        public const int ErrorPrice = -1;
        public const int SandwichPrice = 10;
        public const int NormalDessertPrice = 2;
        public const int SpecialDessertPrice = 4;
        public const int PlatePrice = 15;
        public const int SmallDrinkPrice = 2;
        public const int MediumDrinkPrice = 3;
        public const int GrandDrinkPrice = 4;
        public const int CoffeePrice = 1;
    }
}